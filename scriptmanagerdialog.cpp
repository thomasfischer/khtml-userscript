/*
* Copyright (c) 2007 Mathieu Ducharme <ducharme.mathieu@gmail.com>
*
* See AUTHORS for details
*
* This software is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This software is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; see the file COPYING.
* If not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "scriptmanagerdialog.h"
#include "userscript.h"
#include "userscriptlist.h"
#include "khtml_userscript.h"

#include <qfileinfo.h>


/*
*
*/
ScriptManagerDialog::ScriptManagerDialog()
{
    QWidget *w = new QWidget(this);
    QHBoxLayout *lay = new QHBoxLayout;
    m_ui = new ScriptManager(this);
    lay->addWidget(m_ui);
    w->setLayout(lay);
    setButtons(KDialog::Ok | KDialog::Cancel);
    setMainWidget(w);
    init();

    connect(m_ui->scriptList, SIGNAL(currentRowChanged(int)), this, SLOT(selectScript(int)));
}

/*
*
*/
ScriptManagerDialog::~ScriptManagerDialog()
{

}

/*
*
*/
void ScriptManagerDialog::init()
{

    QListIterator<UserScript*> it(UserScriptList::scriptList());
    while (it.hasNext()) {

        UserScript *uscript = it.next();

        //Add the script to the view widget
        m_ui->scriptList->addItem(uscript->name());
    }

    selectScript(0);
}

/*
*
*/
void ScriptManagerDialog::selectScript(int pos)
{
    if (pos < 0) {
        return;
    }

    m_ui->scriptList->setCurrentRow(pos);

    QList<UserScript*> usl = UserScriptList::scriptList();

    if (pos < usl.size()) {
        UserScript* uscript = usl.at(pos);

        m_ui->nameLabel->setText(i18n("%1 (version %2)", uscript->name(), uscript->version()));
        m_ui->descriptionLabel->setText(uscript->description());
        m_ui->authorLabel->setText(uscript->author());

        m_ui->includedEditListBox->clear();
        m_ui->includedEditListBox->insertStringList(uscript->includes());

        m_ui->excludedEditListBox->clear();
        m_ui->excludedEditListBox->insertStringList(uscript->excludes());
        m_ui->enabledCheckbox->setChecked(true);
    }
}

#include "scriptmanagerdialog.moc"

