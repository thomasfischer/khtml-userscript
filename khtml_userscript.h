/*
* Copyright (c) 2007 Akhmad Fathonih <akhmadf@gmail.com>
* Copyright (c) 2007 Mathieu Ducharme <ducharme.mathieu@gmail.com>
*
* See AUTHORS for details
*
* This software is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This software is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; see the file COPYING.
* If not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef _KHTML_USERSCRIPT_H_
#define _KHTML_USERSCRIPT_H_

#include <userscript.h>

#include <kactionmenu.h>
#include <kconfig.h>
#include <kparts/plugin.h>
#include <khtml_part.h>
#include <kmenu.h>
#include <knewstuff2/engine.h>
#include <kurllabel.h>
#include <kiconloader.h>

#include <QPointer>
#include <qlist.h>

namespace KParts
{
class StatusBarExtension;
}

/**
*
*/
class KHtmlUserscript : public KParts::Plugin
{

    Q_OBJECT

public:

    /**
    * Constructor
    */
    KHtmlUserscript(QObject* parent, const QStringList&);

    /**
    * Destructor
    */
    virtual ~KHtmlUserscript();

    bool drawMenu();

private:

    /**
    *
    */
    QPointer<KHTMLPart> m_part;

    /**
    *
    */
    KActionMenu *m_menu;

    QList<QAction *> m_tmpActions;

    /**
    *
    */
    KConfig *m_cfg;

    /**
    *
    */
    KParts::StatusBarExtension *m_statusBarEx;

    /**
    *
    */
    QPointer<KUrlLabel> m_icon;

    /**
    *
    */
    bool m_enabled;

    /**
    *
    */
    bool m_showIcon;

    /**
    *
    */
    bool m_compatIncluded;

    /**
    *
    */
    void setStatusBarIcon();

    /**
    *
    */
    bool includeCompatScript();



public slots:


    /**
    * Finds the js scripts for this page and executes them
    */
    void slotExecScripts();

    /**
    * Toggle script enabled on/off.
    */
    void slotToggleEnabled();

    /**
    * Toggle script enabled on/off.
    */
    void slotToggleShowIcon();
    /**
    * Displays the script manager dialog
    */
    void slotShowScriptManager();

    /**
    * Display the installer
    */
    void slotShowInstaller();

    /**
    * Creates a (blank) new user script
    */
    void slotShowNewScript();

    /**
    * Displays the download manager //TODO: (GHNS?)
    */
    void slotGetNewScript();

    /**
    * Set the status bar icon
    */
    void slotSetStatusbarIcon();

    /**
    * Display the menu on the icon
    */
    void slotIconMenu();

    /**
    * Remove the statusbar icon
    */
    void slotRemoveStatusbarIcon();
};

#endif // _KHTML_USERSCRIPT_H_


