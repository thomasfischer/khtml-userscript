/*
* Copyright (c) 2007 Mathieu Ducharme <ducharme.mathieu@gmail.com>
*
* See AUTHORS for details
*
* This software is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This software is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; see the file COPYING.
* If not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "userjslineanalyzer.h"

#include <strigi/strigiconfig.h>
#include <strigi/fieldtypes.h>
#include <strigi/analysisresult.h>


using namespace Strigi;
using namespace std;

const string UserJsLineAnalyzerFactory::nameFieldName("userscript.name");
const string UserJsLineAnalyzerFactory::versionFieldName("userscript.version");
const string UserJsLineAnalyzerFactory::namespaceFieldName("userscript.namespace");
const string UserJsLineAnalyzerFactory::authorFieldName("userscript.author");
const string UserJsLineAnalyzerFactory::descriptionFieldName("userscript.description");
const string UserJsLineAnalyzerFactory::includeFieldName("userscript.include");
const string UserJsLineAnalyzerFactory::excludeFieldName("userscript.exclude");

///////////////////////////////////////////////////////////////////////////////
//  USERJSTHROUGHANALYZER
/////////////////////////////////////////////////////////////////////////////

/*
*
*/
void UserJsLineAnalyzer::startAnalysis(Strigi::AnalysisResult* res)
{
    if (res->fileName().find(".user.js", 0) != string::npos) {
        finished = false;
        result = res;
    } else {
        finished = true;
    }

}

/*
*
*/
void UserJsLineAnalyzer::handleLine(const char* data, uint32_t length)
{
    std::string line(data, length);
    uint32_t pos = 0;

    if ((pos = line.find("==/UserScript==", 0)) != string::npos) {
        finished = true;
    } else {
        if ((pos = line.find("@exclude", 0)) != string::npos) {
            result->addValue(factory->excludeField, line.substr(pos + 8, line.size()));
        } else if ((pos = line.find("@include", 0)) != string::npos) {
            result->addValue(factory->includeField, line.substr(pos + 8, line.size()));
        } else if ((pos = line.find("@description", 0)) != string::npos) {
            result->addValue(factory->descriptionField, line.substr(pos + 12, line.size()));
        } else if ((pos = line.find("@author", 0)) != string::npos) {
            result->addValue(factory->authorField, line.substr(pos + 7, line.size()));
        } else if ((pos = line.find("@namespace", 0)) != string::npos) {
            result->addValue(factory->namespaceField, line.substr(pos + 10, line.size()));
        } else if ((pos = line.find("@version", 0)) != string::npos) {
            result->addValue(factory->versionField, line.substr(pos + 8, line.size()));
        } else if ((pos = line.find("@name", 0)) != string::npos) {
            result->addValue(factory->nameField, line.substr(pos + 5, line.size()));
        }
    }
}

/*
*
*/
bool UserJsLineAnalyzer::isReadyWithStream()
{
    return finished;
}


///////////////////////////////////////////////////////////////////////////////
//  USERJSTHROUGHANALYZERFACTORY
///////////////////////////////////////////////////////////////////////////////

/*
*
*/
void UserJsLineAnalyzerFactory::registerFields(FieldRegister& reg)
{
    nameField = reg.registerField(nameFieldName, FieldRegister::stringType, 1, 0);
    versionField = reg.registerField(versionFieldName, FieldRegister::stringType, 1, 0);
    namespaceField = reg.registerField(namespaceFieldName, FieldRegister::stringType, 1, 0);
    authorField = reg.registerField(authorFieldName, FieldRegister::stringType, 1, 0);
    descriptionField = reg.registerField(descriptionFieldName, FieldRegister::stringType, 1, 0);
    includeField = reg.registerField(includeFieldName, FieldRegister::stringType, 0, 0);
    excludeField = reg.registerField(excludeFieldName, FieldRegister::stringType, 0, 0);
}

//Factory
class Factory : public AnalyzerFactoryFactory
{
public:
    list<StreamLineAnalyzerFactory*>
    streamLineAnalyzerFactories() const {
        list<StreamLineAnalyzerFactory*> af;
        af.push_back(new UserJsLineAnalyzerFactory());
        return af;
    }
};

STRIGI_ANALYZER_FACTORY(Factory)
