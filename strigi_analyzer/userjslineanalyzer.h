/*
* Copyright (c) 2007 Mathieu Ducharme <ducharme.mathieu@gmail.com>
*
* See AUTHORS for details
*
* This software is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This software is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; see the file COPYING.
* If not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef USERJSLINEANALYZER_H
#define USERJSLINEANALYZER_H

#include <strigi/streamlineanalyzer.h>
#include <strigi/analyzerplugin.h>
#include <strigi/analysisresult.h>
#include <string>

class UserJsLineAnalyzerFactory;

/**
*
*/
class UserJsLineAnalyzer : public Strigi::StreamLineAnalyzer
{
public:
    /**
    * Constructor
    */
    UserJsLineAnalyzer(const UserJsLineAnalyzerFactory* f) : factory(f) {}

    /**
    * Destructor
    */
    ~UserJsLineAnalyzer() {}

private:

    /**
    *
    */
    Strigi::AnalysisResult* indexable;

    /**
    *
    */
    const UserJsLineAnalyzerFactory* factory;

    /**
    *
    */
    bool finished;

    /**
    *
    */
    Strigi::AnalysisResult* result;

    /**
    *
    */
    virtual void startAnalysis(Strigi::AnalysisResult*);

    /**
    *
    */
    virtual void handleLine(const char* data, uint32_t length);

    /**
    *
    */
    virtual void endAnalysis(bool /* complete */) {};

    /**
    *
    */
    virtual bool isReadyWithStream();

    /**
    *
    */
    const char* name() const {
        return "UserJsLineAnalyzer";
    }
};

/**
*
*/
class UserJsLineAnalyzerFactory : public Strigi::StreamLineAnalyzerFactory
{

    friend class UserJsLineAnalyzer;

private:

    static const std::string nameFieldName;
    static const std::string versionFieldName;
    static const std::string namespaceFieldName;
    static const std::string authorFieldName;
    static const std::string descriptionFieldName;
    static const std::string includeFieldName;
    static const std::string excludeFieldName;

    const Strigi::RegisteredField* nameField;
    const Strigi::RegisteredField* versionField;
    const Strigi::RegisteredField* namespaceField;
    const Strigi::RegisteredField* authorField;
    const Strigi::RegisteredField* descriptionField;
    const Strigi::RegisteredField* includeField;
    const Strigi::RegisteredField* excludeField;

    /**
    *
    */
    const char* name() const {
        return "UserJsLineAnalyzer";
    }


    Strigi::StreamLineAnalyzer* newInstance() const {
        return new UserJsLineAnalyzer(this);
    }

    void registerFields(Strigi::FieldRegister&);
};

#endif
