/*
* Copyright (c) 2012 Thomas Fischer <fischer@unix-ag.uni-kl.de>
*
* See AUTHORS for details
*
* This software is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This software is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; see the file COPYING.
* If not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include <QLabel>
#include <QCheckBox>
#include <QWidget>
#include <QTextEdit>
#include <QLayout>

#include <KEditListWidget>
#include <KGlobalSettings>
#include <KLocale>
#include <KIcon>

#include "userscript.h"
#include "scriptviewerwidget.h"

ScriptViewerWidget::ScriptViewerWidget(QWidget *parent)
    : QTabWidget(parent), m_userScript(NULL)
{
    setupGUI();
}

void ScriptViewerWidget::setScript(UserScript *userScript)
{
    m_userScript = userScript;

    m_listWidgetIncludes->clear();
    m_listWidgetExcludes->clear();
    if (m_userScript != NULL) {
        m_labelName->setText(m_userScript->name());
        const QString version = m_userScript->version();
        m_labelVersion->setText(version.isEmpty() ? QString::null : i18n("(version %1)", version));
        const QString author = m_userScript->author();
        m_labelAuthor->setText(author.isEmpty() ? i18n("No author specified") : author);
        const QString description = m_userScript->description();
        m_labelDescription->setText(description.isEmpty() ? i18n("No description available.") : m_userScript->description());
        m_checkBoxEnabled->setChecked(m_userScript->isEnabled());
        m_listWidgetIncludes->insertStringList(m_userScript->includes());
        m_listWidgetExcludes->insertStringList(m_userScript->excludes());
        m_textEditSourceCode->setText(m_userScript->script());
    } else {
        m_labelName->setText(QString::null);
        m_labelVersion->setText(QString::null);
        m_labelAuthor->setText(QString::null);
        m_labelDescription->setText(QString::null);
        m_checkBoxEnabled->setChecked(false);
        m_textEditSourceCode->setText(QString::null);
    }
}

void ScriptViewerWidget::apply()
{
    if (m_userScript != NULL) {
        m_userScript->setIncludesExcludes(m_listWidgetIncludes->items(), m_listWidgetExcludes->items());
    }
}

void ScriptViewerWidget::setupGUI()
{
    /// General tab ==========================================
    QWidget *container = new QWidget(this);
    addTab(container, i18n("General"));
    QBoxLayout *vboxLayout = new QVBoxLayout(container);

    QBoxLayout *hboxLayout = new QHBoxLayout();
    vboxLayout->addLayout(hboxLayout, 0);
    m_labelName = new QLabel(container);
    hboxLayout->addWidget(m_labelName, 0);
    QFont font = m_labelName->font();
    font.setBold(true);
    m_labelName->setFont(font);
    hboxLayout->addSpacing(4);
    m_labelVersion = new QLabel(container);
    hboxLayout->addWidget(m_labelVersion, 0);
    hboxLayout->addStretch(10);

    vboxLayout->addSpacing(4);

    m_labelAuthor = new QLabel(container);
    vboxLayout->addWidget(m_labelAuthor, 0);

    vboxLayout->addSpacing(4);

    m_labelDescription = new QLabel(container);
    vboxLayout->addWidget(m_labelDescription, 0);
    m_labelDescription->setWordWrap(true);

    vboxLayout->addStretch(10);

    m_checkBoxEnabled = new QCheckBox(i18n("Enable this script"));
    vboxLayout->addWidget(m_checkBoxEnabled, 0);

    /// Includes/Excludes tab ==========================================
    container = new QWidget(this);
    addTab(container, i18n("Includes/Excludes"));
    vboxLayout = new QVBoxLayout(container);

    QLabel *label = new QLabel(i18n("Include URLs matching those rules:"), container);
    vboxLayout->addWidget(label, 0);
    m_listWidgetIncludes = new KEditListWidget(container);
    const int h = m_listWidgetIncludes->fontMetrics().height();
    m_listWidgetIncludes->setMinimumHeight(h * 10);
    m_listWidgetIncludes->layout()->setMargin(0);
    vboxLayout->addWidget(m_listWidgetIncludes, 1);
    label->setBuddy(m_listWidgetIncludes);

    vboxLayout->addSpacing(4);

    label = new QLabel(i18n("Exclude URLs matching those rules:"), container);
    vboxLayout->addWidget(label, 0);
    m_listWidgetExcludes = new KEditListWidget(container);
    m_listWidgetExcludes->setMinimumHeight(h * 10);
    m_listWidgetExcludes->layout()->setMargin(0);
    vboxLayout->addWidget(m_listWidgetExcludes, 1);
    label->setBuddy(m_listWidgetExcludes);

    /// Source Code tab ==========================================
    container = new QWidget(this);
    addTab(container, i18n("Source Code"));
    vboxLayout = new QVBoxLayout(container);

    m_textEditSourceCode = new QTextEdit(container);
    vboxLayout->addWidget(m_textEditSourceCode, 1);
    m_textEditSourceCode->setReadOnly(true);
    m_textEditSourceCode->setAcceptRichText(false);
    m_textEditSourceCode->setFont(KGlobalSettings::fixedFont());
    const int w = m_textEditSourceCode->fontMetrics().width(QChar('W'));
    m_textEditSourceCode->setMinimumSize(w * 80, w * 40);
}
