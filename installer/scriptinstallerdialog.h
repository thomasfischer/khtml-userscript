/*
* Copyright (c) 2007 Mathieu Ducharme <ducharme.mathieu@gmail.com>
*
* See AUTHORS for details
*
* This software is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This software is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; see the file COPYING.
* If not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/
#ifndef _SCRIPTINSTALLER_H_
#define _SCRIPTINSTALLER_H_

#include <KDialog>

class UserScript;
class ScriptViewerWidget;

/**
*
*/
class ScriptInstallerDialog : public KDialog
{
    Q_OBJECT
public:

    /**
    *
    */
    ScriptInstallerDialog(UserScript* uscript);

public slots:

    /**
    *
    */
    bool installScript();

private:
    /**
    *
    */
    UserScript* m_script;

    ScriptViewerWidget *m_widget;
};

#endif // _SCRIPTINSTALLER_H_
