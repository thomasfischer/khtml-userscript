/*
* Copyright (c) 2007 Mathieu Ducharme <ducharme.mathieu@gmail.com>
*
* See AUTHORS for details
*
* This software is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This software is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; see the file COPYING.
* If not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "../userscript.h"
#include "scriptinstallerdialog.h"

#include <QString>
#include <QTextStream>
#include <QFileInfo>
#include <QFile>
#include <QRegExp>
#include <QDir>

#include <KApplication>
#include <KAboutData>
#include <KMessageBox>
#include <KCmdLineArgs>
#include <KLocale>
#include <KDebug>
#include <KStandardDirs>
#include <KFileDialog>

class UserScript;

int main(int argc, char *argv[])
{
    KAboutData aboutData("khtml_userscript", 0, ki18n("khtml_userscript"), "0.1", ki18n("KHTML Userscript Installer"), KAboutData::License_GPL, ki18n("(c) 2007"));
    aboutData.addAuthor(ki18n("Mathieu Ducharme"), ki18n("Main developer and maintainer"), "ducharme.mathieu@gmail.com");

    KCmdLineArgs::init(argc, argv, &aboutData);

    KCmdLineOptions cmdLineOptions;
    cmdLineOptions.add("+url",  ki18n("Location of the userscript to install"));
    cmdLineOptions.add("force", ki18n("Force installation (do not validate userscript)"));
    KCmdLineArgs::addCmdLineOptions(cmdLineOptions);
    KCmdLineArgs *args = KCmdLineArgs::parsedArgs();

    QString url;

    KApplication app;
    if (args->count() > 0) {
        url = args->arg(0);
        kDebug() << "URL: " << url << "\n";
    } else {

        url = KFileDialog::getOpenFileName(KUrl(), i18n("*.user.js|Userscripts"), 0, i18n("Select Userscript file"));

    }

    if (url.isEmpty()) {
        kDebug() << "You need to specify a url. Aborting.";
        exit(0);
    }

    UserScript *script = new UserScript(url);

    if (!args->isSet("force") && !script->isValid()) {
        // TODO: Write the isValid function in userscript class!
        KMessageBox::error(0, i18n("The userscript '%1' does not appear to be a valid userscript.", script->filename()));
        delete script;
        exit(0);
    }

    ScriptInstallerDialog* dlg = new ScriptInstallerDialog(script);
    dlg->exec();
    delete dlg;

    delete script;
}
