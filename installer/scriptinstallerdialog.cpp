/*
* Copyright (c) 2007 Mathieu Ducharme <ducharme.mathieu@gmail.com>
*
* See AUTHORS for details
*
* This software is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This software is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; see the file COPYING.
* If not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include <KLocale>
#include <KPushButton>

#include "../userscript.h"
#include "../userscriptlist.h"
#include "../userscript.h"
#include "../scriptviewerwidget.h"
#include "scriptinstallerdialog.h"

ScriptInstallerDialog::ScriptInstallerDialog(UserScript* script)
{
    m_widget = new ScriptViewerWidget(this);
    setMainWidget(m_widget);
    m_script = script;
    m_widget->setScript(m_script);
    if (!m_script->name().isEmpty())
        setCaption(m_script->name());

    button(KDialog::Ok)->setText(i18n("Install"));

    connect(this, SIGNAL(accepted()), this, SLOT(installScript()));
}

bool ScriptInstallerDialog::installScript()
{
    m_widget->apply();
    return m_script->install();
}
