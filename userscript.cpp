/*
* Copyright (c) 2007 Mathieu Ducharme <ducharme.mathieu@gmail.com>
*
* See AUTHORS for details
*
* This software is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This software is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; see the file COPYING.
* If not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "userscript.h"

#include <qwidget.h>
#include <qstring.h>
#include <qstringlist.h>
#include <qlist.h>
#include <qfileinfo.h>
#include <qfile.h>
#include <qdir.h>
#include <qtextstream.h>
#include <qregexp.h>

#include <KTempDir>
#include <KTemporaryFile>
#include <KIO/NetAccess>
#include <KMessageBox>
#include <KLocale>

#include <kconfig.h>
#include <kconfiggroup.h>
#include <kdebug.h>
#include <kstandarddirs.h>

#include "userscriptlist.h"

/*
*
*/
UserScript::UserScript(const KUrl &url)
    : m_isLoaded(false), m_isEnabled(true), m_isValid(true /* TODO */)
{
    loadFromUrl(url);
}

/*
*
*/
UserScript::~UserScript()
{

}

bool UserScript::install()
{
    const QString installedFilename = KStandardDirs::locateLocal("data", "konqueror/userscripts/", true) + m_filename;
    QFile file(installedFilename);
    if (file.open(KTemporaryFile::WriteOnly)) {
        QTextStream stream(&file);
        stream.setCodec("utf-8");
        stream << m_scriptRawText;
        stream.flush();
        file.close();

        KMessageBox::information(0, i18n("Script successfully installed. You will need to restart Konqueror"), i18n("Userscript installed"));
        QList<UserScript*> usl = UserScriptList::scriptList();
        usl << this;
        return true;
    } else {
        kError() << "Could not save script as file" << file.fileName();
        KMessageBox::error(0, i18n("Script could not be saved. Please make sure a script with the same name is not already installed"), i18n("Could not install Userscript"));
        return false;
    }
}

/*
*
*/
const QString UserScript::filename() const
{
    return m_filename;
}

/*
*
*/
const QString UserScript::name() const
{
    return m_name;
}

/*
*
*/
const QString UserScript::version() const
{
    return m_version;
}

/*
*
*/
const QString UserScript::ns() const
{
    return m_namespace;
}

/*
*
*/
const QString UserScript::author() const
{
    return m_author;
}

/*
*
*/
const QString UserScript::description() const
{
    return m_description;
}

/*
*
*/
const QStringList UserScript::includes() const
{
    return m_includes;
}

void UserScript::setIncludes(const QStringList &newIncludes)
{
    static const QRegExp includeLineRegExp(QLatin1String("//\\s*@include\\s+"));
    static const QRegExp endOfHeaderRegExp(QLatin1String("//\\s*==/UserScript=="));
    QStringList scriptLines = m_scriptRawText.split(QChar('\n'));

    QStringList newScriptLines;
    bool forceAppendEndOfHeaderLine = false;
    foreach(const QString & line, scriptLines) {
        if (endOfHeaderRegExp.indexIn(line) == 0) {
            forceAppendEndOfHeaderLine = true;
            break;
        } else if (includeLineRegExp.indexIn(line) != 0)
            newScriptLines.append(line);
        else
            break;
    }

    foreach(const QString & newInclude, newIncludes) {
        newScriptLines.append(QString(QLatin1String("// @include %1")).arg(newInclude));
    }

    if (forceAppendEndOfHeaderLine)
        newScriptLines.append(QLatin1String("// ==/UserScript=="));

    foreach(const QString & line, scriptLines) {
        if (includeLineRegExp.indexIn(line) != 0)
            newScriptLines.append(line);
    }

    m_scriptRawText = newScriptLines.join(QLatin1String("\n"));
}

/*
*
*/
const QStringList UserScript::excludes() const
{
    return m_excludes;
}

void UserScript::setExcludes(const QStringList &newExcludes)
{
    static const QRegExp excludeLineRegExp(QLatin1String("//\\s*@exclude\\s+"));
    static const QRegExp endOfHeaderRegExp(QLatin1String("//\\s*==/UserScript=="));
    QStringList scriptLines = m_scriptRawText.split(QChar('\n'));

    QStringList newScriptLines;
    bool forceAppendEndOfHeaderLine = false;
    foreach(const QString & line, scriptLines) {
        if (endOfHeaderRegExp.indexIn(line) == 0) {
            forceAppendEndOfHeaderLine = true;
            break;
        } else if (excludeLineRegExp.indexIn(line) != 0)
            newScriptLines.append(line);
        else
            break;
    }

    foreach(const QString & newExclude, newExcludes) {
        newScriptLines.append(QString(QLatin1String("// @exclude %1")).arg(newExclude));
    }

    if (forceAppendEndOfHeaderLine)
        newScriptLines.append(QLatin1String("// ==/UserScript=="));

    foreach(const QString & line, scriptLines) {
        if (excludeLineRegExp.indexIn(line) != 0)
            newScriptLines.append(line);
    }

    m_scriptRawText = newScriptLines.join(QLatin1String("\n"));
}

void UserScript::setIncludesExcludes(const QStringList &newIncludes, const QStringList &newExcludes)
{
    static const QRegExp inexcludeLineRegExp(QLatin1String("//\\s*@(ex|in)clude\\s+"));
    static const QRegExp endOfHeaderRegExp(QLatin1String("//\\s*==/UserScript=="));
    QStringList scriptLines = m_scriptRawText.split(QChar('\n'));

    QStringList newScriptLines;
    bool forceAppendEndOfHeaderLine = false;
    foreach(const QString & line, scriptLines) {
        if (endOfHeaderRegExp.indexIn(line) == 0) {
            forceAppendEndOfHeaderLine = true;
            break;
        } else if (inexcludeLineRegExp.indexIn(line) != 0)
            newScriptLines.append(line);
        else
            break;
    }

    foreach(const QString & newInclude, newIncludes) {
        newScriptLines.append(QString(QLatin1String("// @include %1")).arg(newInclude));
    }

    foreach(const QString & newExclude, newExcludes) {
        newScriptLines.append(QString(QLatin1String("// @exclude %1")).arg(newExclude));
    }

    if (forceAppendEndOfHeaderLine)
        newScriptLines.append(QLatin1String("// ==/UserScript=="));

    foreach(const QString & line, scriptLines) {
        if (inexcludeLineRegExp.indexIn(line) != 0)
            newScriptLines.append(line);
    }

    m_scriptRawText = newScriptLines.join(QLatin1String("\n"));
}

/*
*
*/
const QString UserScript::script() const
{
    return m_scriptRawText;
}

/*
*
*/
bool UserScript::isEnabled() const
{
    return m_isEnabled && m_isLoaded && m_isValid;
}

/*
*
*/
bool UserScript::isValid() const
{
    return m_isValid && m_isLoaded;
}

bool UserScript::loadFromUrl(const KUrl &url)
{
    QString internalFilename;
    KTempDir d;
    d.setAutoRemove(true);

    if (url.isLocalFile())
        internalFilename = url.toLocalFile();
    else {
        internalFilename = url.fileName();
        internalFilename = internalFilename.prepend(d.name() + QDir::separator());
        const bool success = KIO::NetAccess::copy(url, KUrl::fromLocalFile(internalFilename), NULL /* FIXME */);
        if (!success)
            return false; ///< download failed
    }

    return loadFromFile(internalFilename);
}

bool UserScript::loadFromFile(const QString &localFilename)
{
    m_scriptRawText = QString::null;

    QFile file(localFilename);

    if (!file.open(QIODevice::ReadOnly)) {
        kError() << "Could not open file " << m_filename;
        return (m_isLoaded = false);
    }

    m_filename = QFileInfo(file).fileName();
    kDebug() << "m_filename=" << m_filename;

    // Let's read the content of the file
    QTextStream stream(&file);
    stream.setCodec("utf-8");

    m_name = QString::null;
    m_version = QString::null;
    m_namespace = QString::null;
    m_author = QString::null;
    m_description = QString::null;
    m_includes.clear();
    m_excludes.clear();
    bool insideHeader = false;
    QString line = QString::null;
    while (!stream.atEnd() && !(line = stream.readLine()).isNull()) {
        if (!m_scriptRawText.isEmpty()) m_scriptRawText = m_scriptRawText.append(QChar('\n'));
        m_scriptRawText = m_scriptRawText.append(line);

        static const QRegExp scriptHeaderRegExp(QLatin1String("//\\s+(==[/]?UserScript==)"));
        if (scriptHeaderRegExp.indexIn(line) >= 0) {
            insideHeader = scriptHeaderRegExp.cap(1) == QLatin1String("==UserScript==");
            continue;
        }

        // ==UserScript==
        // @name        Plugin Name
        // @version     x.x+
        // @namespace       http://greasemonkey.example.com
        // @author      John Smith
        // @description     Provides an example header for userscripts
        // @include     *
        // @exclude     *.example.com*
        // @exclude     *.test.com/*
        // ==/UserScript==

        if (insideHeader) {
            static const QRegExp headerKeyValueRegExp(QLatin1String("//\\s*(@\\S+)\\s+(.+)$"));
            if (headerKeyValueRegExp.indexIn(line) >= 0) {
                const QString key = headerKeyValueRegExp.cap(1).toLower();
                const QString value = headerKeyValueRegExp.cap(2).trimmed();
                kDebug() << "Read key-value pair in script header:" << key << "=" << value;

                if (key == QLatin1String("@name"))
                    m_name = value;
                else if (key == QLatin1String("@version"))
                    m_version = value;
                else if (key == QLatin1String("@namespace"))
                    m_namespace = value;
                else if (key == QLatin1String("@author"))
                    m_author = value;
                else if (key == QLatin1String("@description")) {
                    if (!m_description.isEmpty()) m_description = m_description.append(QChar('\n'));
                    m_description = m_description.append(value);
                } else if (key == QLatin1String("@include"))
                    m_includes.append(value);
                else if (key == QLatin1String("@exclude"))
                    m_excludes.append(value);
                else
                    kDebug() << "Unknown key-value pair in script header:" << key << "=" << value;
            }
        }

    }
    file.close();

    if (m_scriptRawText.isEmpty()) {
        kError() << "File is empty: " << m_filename;
        return (m_isLoaded = false);
    }

    if (m_name.isEmpty()) {
        kDebug() << "File " << m_filename << "does not appear to be a valid userscript, some header fields are missing";
        return (m_isLoaded = false);
    }

    return (m_isLoaded = true);
}

/*
*
*/
bool UserScript::matchUrl(const QString & url)
{
    QRegExp re;

    re.setPatternSyntax(QRegExp::Wildcard);
    //re.setWildcard(true);

    bool included = false;
    bool excluded = false;

    for (QStringList::Iterator inc_it = m_includes.begin(); inc_it != m_includes.end(); ++inc_it) {
        re.setPattern(*inc_it);
        if (re.exactMatch(url)) {
            included = true;
            break;
        }
    }

    for (QStringList::Iterator exc_it = m_excludes.begin(); exc_it != m_excludes.end(); ++exc_it) {
        re.setPattern(*exc_it);
        if (re.exactMatch(url)) {
            excluded = true;
            break;
        }
    }

    if (included && !excluded) {
        kDebug() << "Userscript [" << m_filename << "] successfully met url \"" << url << "\" include/exclude rules\n";
        return true;
    } else {
        kDebug() << "Userscript [" << m_filename << "] did not meet url \"" << url << "\"include/exclude rules\n";
        return false;
    }
}

/*
*
*/
void UserScript::slotEnable(bool chkValue)
{
    m_isEnabled = chkValue;

    KConfig* kconfig = new KConfig("khtml_userscriptrc");
    KConfigGroup grp(kconfig, m_filename);
    grp.writeEntry("enabled", chkValue);
    kconfig->sync();
    delete kconfig;
}

#include "userscript.moc"

