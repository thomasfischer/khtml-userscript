/*
* Copyright (c) 2007 Mathieu Ducharme <ducharme.mathieu@gmail.com>
*
* See AUTHORS for details
*
* This software is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This software is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; see the file COPYING.
* If not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "userscriptlist.h"
#include "userscript.h"

class UserScriptListSingleton
{
public:
    UserScriptList self;
};

K_GLOBAL_STATIC(UserScriptListSingleton, usList)

/*
*
*/
UserScriptList* UserScriptList::self()
{
    return &usList->self;
}

/*
*
*/
UserScriptList::UserScriptList()
{
    QDir dir(KStandardDirs::locateLocal("data", "konqueror/userscripts/", true));
    dir.setFilter(QDir::Files | QDir::NoSymLinks);
    dir.setSorting(QDir::Size | QDir::Reversed);

    QStringList filters;
    filters << QLatin1String("*.user.js");
    dir.setNameFilters(filters);

    QDirIterator it(dir);

    while (it.hasNext()) {
        it.next();
        m_scriptList.append(new UserScript(KUrl::fromLocalFile(it.fileInfo().absoluteFilePath())));
    }

}

/*
*
*/
UserScriptList::~UserScriptList()
{

}

/*
*
*/
QList<UserScript *> UserScriptList::scriptList()
{
    UserScriptList* usl = UserScriptList::self();
    return usl->m_scriptList;
}

#include "userscriptlist.moc"
