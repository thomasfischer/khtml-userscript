/*
* Copyright (c) 2007 Mathieu Ducharme <ducharme.mathieu@gmail.com>
*
* See AUTHORS for details
*
* This software is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This software is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; see the file COPYING.
* If not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef _USERSCRIPT_H_
#define _USERSCRIPT_H_

#include <qwidget.h>
#include <qstringlist.h>
#include <qlist.h>
#include <qfileinfo.h>

#include <KUrl>

/**
*
*/
class UserScript : public QWidget
{
    Q_OBJECT

public:
    /**
    * Constructor
    */
    UserScript(const KUrl &url);

    /**
    * Destructor
    */
    ~UserScript();

    bool install();

    const QString filename() const;

    const QString name() const;
    const QString version() const;
    const QString ns() const;
    const QString author() const;
    const QString description() const;

    const QStringList includes() const;
    void setIncludes(const QStringList &newIncludes);
    const QStringList excludes() const;
    void setExcludes(const QStringList &newExcludes);
    void setIncludesExcludes(const QStringList &newIncludes, const QStringList &newExcludes);

    const QString script() const;

    /**
     * Check if this script should be used or is deactivated.
     * @return true if script should be used, ignore otherwise
     */
    bool isEnabled() const;

    /**
    * Test wether the script includes/excludes rules match the url given in parameter
    */
    bool matchUrl(const QString & url);

    /**
    * Try to find if a script is valid or not
    */
    bool isValid() const;

public slots:

    void slotEnable(bool);

private:
    /** Filename of the script */
    QString m_filename;

    /** The name of the script */
    QString m_name;
    /** Version number string */
    QString m_version;
    /** Namespace URL */
    QString m_namespace;
    /** Script author */
    QString m_author;
    /** Short description of the script */
    QString m_description;

    /** List of wildcard that the URL should match to be executed */
    QStringList m_includes;
    /** List of wildcard that will disallow execution if they match the URL */
    QStringList m_excludes;

    /**
    * The content of the script itself
    */
    QString m_scriptRawText;

    /** Script got correctly loaded? */
    bool m_isLoaded;

    /** Is this script currently enabled or disabled? */
    bool m_isEnabled;

    /** Is this script valid? */
    bool m_isValid;

    bool loadFromUrl(const KUrl &url);

    /**
    * Read in the script information.
    * m_isLoaded is automatically set to the return value
    */
    bool loadFromFile(const QString &localFilename);
};



#endif // _USERSCRIPT_H_

