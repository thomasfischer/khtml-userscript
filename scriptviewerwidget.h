/*
* Copyright (c) 2012 Thomas Fischer <fischer@unix-ag.uni-kl.de>
*
* See AUTHORS for details
*
* This software is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This software is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; see the file COPYING.
* If not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef SCRIPTVIEWERWIDGET_H
#define SCRIPTVIEWERWIDGET_H

#include <QTabWidget>

class QLabel;
class QCheckBox;
class QTextEdit;
class KEditListWidget;

class UserScript;

class ScriptViewerWidget : public QTabWidget
{
    Q_OBJECT
public:
    ScriptViewerWidget(QWidget *parent);

    void setScript(UserScript *userScript);

public slots:
    void apply();

private:
    UserScript *m_userScript;

    QLabel *m_labelName, *m_labelVersion, *m_labelAuthor, *m_labelDescription;
    QCheckBox *m_checkBoxEnabled;
    QTextEdit *m_textEditSourceCode;
    KEditListWidget *m_listWidgetIncludes, *m_listWidgetExcludes;

    void setupGUI();
};

#endif // SCRIPTVIEWERWIDGET_H
