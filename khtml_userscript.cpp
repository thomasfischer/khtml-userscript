/*
* Copyright (c) 2007 Akhmad Fathonih <akhmadf@gmail.com>
* Copyright (c) 2007 Mathieu Ducharme <ducharme.mathieu@gmail.com>
*
* See AUTHORS for details
*
* This software is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This software is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; see the file COPYING.
* If not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "khtml_userscript.h"
#include "scriptmanagerdialog.h"
#include "userscriptlist.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QCursor>

#include <KActionCollection>
#include <KToggleAction>
#include <kconfig.h>
#include <khtml_part.h>
#include <khtmlview.h>
#include <kaction.h>
#include <kmessagebox.h>
#include <kgenericfactory.h>
#include <kurl.h>
#include <dom/html_document.h>
#include <kparts/statusbarextension.h>
#include <kstatusbar.h>
#include <knewstuff2/engine.h>
#include <KRun>

/*
*
*/
KHtmlUserscript::KHtmlUserscript(QObject* parent, const QStringList&)
    : Plugin(parent)
{
    kDebug() << "Initiating USERSCRIPT KHtml Plugin...";

    m_icon = 0;

    // Get the HTML Part
    m_part = dynamic_cast<KHTMLPart *>(parent);
    if (!m_part) {
        kDebug() << "KHTML Userscript: Could not load KHTML Part";
        m_enabled = false;
        return;
    }

    //TODO: Use a local config file. (how? -- this uses konquerorrc)
    //KConfig *kconfig = KGlobal::config();
    m_cfg = new KConfig("khmtl_userscriptrc");
    KConfigGroup grp(m_cfg, "General Settings");

    m_enabled = grp.readEntry("enabled", true);

    m_showIcon = grp.readEntry("showIcon", true);

    kDebug() << "KHTML Userscript Enabled: " << m_enabled;

    m_menu = new KActionMenu(KIcon("konqueror"), i18n("User Scripts"), actionCollection());
    actionCollection()->addAction("khtml_userscript_menu", m_menu);

    // Enabled
    QAction *toggleEnabledAction = actionCollection()->addAction("toggle_enabled");
    toggleEnabledAction->setCheckable(true);
    toggleEnabledAction->setChecked(m_enabled);
    toggleEnabledAction->setText(i18n("&Enabled"));
    connect(toggleEnabledAction, SIGNAL(triggered()), SLOT(slotToggleEnabled()));
    m_menu->addAction(toggleEnabledAction);

    m_menu->addSeparator();

    // Manage user scripts
    QAction *showManagerAction = actionCollection()->addAction("show_scriptmanager");
    showManagerAction->setText(i18n("&Manage User Scripts..."));
    connect(showManagerAction, SIGNAL(triggered()), SLOT(slotShowScriptManager()));
    m_menu->addAction(showManagerAction);

    // Install a script
    QAction *showInstallerAction = actionCollection()->addAction("show_installer");
    showInstallerAction->setText(i18n("&Install Script..."));
    connect(showInstallerAction, SIGNAL(triggered()), SLOT(slotShowInstaller()));
    m_menu->addAction(showInstallerAction);

    // New user script
    QAction *showNewScriptAction = actionCollection()->addAction("show_newscript");
    showNewScriptAction->setText(i18n("&Create A New Script..."));
    connect(showNewScriptAction, SIGNAL(triggered()), SLOT(slotShowNewScript()));
    m_menu->addAction(showNewScriptAction);

    // Get Hot New Script
    QAction *showGetScriptAction = actionCollection()->addAction("show_newscript");
    showGetScriptAction->setText(i18n("Get &New Scripts..."));
    connect(showGetScriptAction, SIGNAL(triggered()), SLOT(slotGetNewScript()));
    m_menu->addAction(showGetScriptAction);

    m_menu->addSeparator();

    // Toggle Statusbar Icon
    QAction *showToggleIconAction = actionCollection()->addAction("toggle_showIcon");
    showToggleIconAction->setCheckable(true);
    showToggleIconAction->setChecked(m_showIcon);
    showToggleIconAction->setText(i18n("&Show icon in status bar"));
    connect(showToggleIconAction, SIGNAL(triggered()), SLOT(slotToggleShowIcon()));
    m_menu->addAction(showToggleIconAction);

    // Execute the scripts after the page is completely loaded
    connect(m_part, SIGNAL(completed()), this, SLOT(slotExecScripts()));
    connect(m_part, SIGNAL(completed()), this, SLOT(slotSetStatusbarIcon()));

    connect(m_part, SIGNAL(started(KIO::Job *)), this, SLOT(slotRemoveStatusbarIcon()));

    m_compatIncluded = false;
}


/*
*
*/
KHtmlUserscript::~KHtmlUserscript()
{
    kDebug() << "Shutting down KHTML Userscript plugin...";

    delete m_menu;
    delete m_cfg;
    delete m_icon;
}

/*
*
*/
void KHtmlUserscript::slotExecScripts()
{

    if (!m_enabled) {
        kDebug() << "User scripts are disabled. Not processing any script(s)";
        return;
    }

    //FIXME
    for (int i = 0; i < m_tmpActions.size(); ++i) {
        QAction* act = m_tmpActions.at(i);
        kDebug() << "Removing entry " << act;
        m_menu->removeAction(act);
    }

    QListIterator<UserScript*> i(UserScriptList::scriptList());
    while (i.hasNext()) {

        UserScript *uscript = i.next();

        kDebug() << "Processing user script:" << uscript->filename();
        //kDebug() << "Script: " << uscript->script();

        if (uscript->matchUrl(m_part->url().url())) {

            if (uscript->isEnabled()) {

                // Make sure the Greasemonkey-compatibility script is included
                includeCompatScript();

                kDebug() << "Script" << uscript->filename() << "will be executed";

                QVariant res = m_part->executeScript(m_part->htmlDocument(), uscript->script());
                kDebug() << "Res: " << res;
            } else {
                kDebug() << "User Script \"" << uscript->filename() << "\" is disabled...";
            }

        }

    }

    drawMenu();

}

/*
*
*/
bool KHtmlUserscript::drawMenu()
{
    m_tmpActions << m_menu->addSeparator();

    QListIterator<UserScript*> i(UserScriptList::scriptList());
    while (i.hasNext()) {

        UserScript *uscript = i.next();

        if (uscript->matchUrl(m_part->url().url())) {
            QAction *scriptAction = actionCollection()->addAction(uscript->filename());
            scriptAction->setCheckable(true);
            scriptAction->setChecked(true);
            scriptAction->setText((!uscript->name().isNull() ? uscript->name() : uscript->filename()));
            connect(scriptAction, SIGNAL(triggered(bool)), uscript, SLOT(slotEnable(bool)));
            m_menu->addAction(scriptAction);
            m_tmpActions << scriptAction;
        }
    }
    return true;
}

/*
*
*/
bool KHtmlUserscript::includeCompatScript()
{
    if (m_compatIncluded) {
        return false;
    }

    KStandardDirs *ksd = new KStandardDirs();
    QString path = ksd->findResource("data", "konqueror/khtml_userscript_gm_compat.js");
    delete ksd;
    kDebug() << "PATH:" << path;

    UserScript *compat = new UserScript(path);

    QVariant res = m_part->executeScript(m_part->htmlDocument(), compat->script());
    kDebug() << "Res: " << res;

    delete compat;

    m_compatIncluded = res.toBool();
    return m_compatIncluded;

}

/*
*
*/
void KHtmlUserscript::slotToggleEnabled()
{
    m_enabled = !m_enabled;

    KConfigGroup grp(m_cfg, "General Settings");

    grp.writeEntry("enabled", m_enabled);
    m_cfg->sync();

    if (m_icon && m_statusBarEx) {
        m_statusBarEx->removeStatusBarItem(m_icon);
    }

    // Reload the document
    KUrl url = m_part->url();
    m_part->closeUrl();
    m_part->openUrl(url);
}

/*
*
*/
void KHtmlUserscript::slotToggleShowIcon()
{
    m_showIcon = !m_showIcon;

    KConfigGroup grp(m_cfg, "General Settings");

    grp.writeEntry("showIcon", m_showIcon);
    m_cfg->sync();

    if (m_showIcon) {
        m_statusBarEx->addStatusBarItem(m_icon, 0, true);
    } else {
        m_statusBarEx->removeStatusBarItem(m_icon);
    }
}


/*
*
*/
void KHtmlUserscript::slotShowScriptManager()
{
    ScriptManagerDialog* smd = new ScriptManagerDialog();
    smd->exec();
    delete smd;
}

/*
*
*/
void KHtmlUserscript::slotShowInstaller()
{
    KRun::runCommand("khtml_userscript_installer", 0);
}


/*
*
*/
void KHtmlUserscript::slotShowNewScript()
{
    KMessageBox::information(0, i18n("New script creation is not (yet) available in this version."), i18n("Konqueror User Scripts Manager"));
}

/*
*
*/
void KHtmlUserscript::slotGetNewScript()
{

    //KNS::Entry::List entries = KNS::Engine::download();
    KMessageBox::information(0, i18n("Get Hot New Script is not (yet) available in this version."), i18n("Konqueror User Scripts Manager"));
}

/*
*
*/
void KHtmlUserscript::slotSetStatusbarIcon()
{
    if (!m_showIcon) {
        return;
    }

    // Get the status bar
    m_statusBarEx = KParts::StatusBarExtension::childObject(m_part);
    if (!m_statusBarEx) {
        kDebug() << "KHTML Userscript: Could not load Status Bar Extension";
        return;
    }

    m_icon = new KUrlLabel(m_statusBarEx->statusBar());

    int iconSize = KIconLoader::global()->currentSize(KIconLoader::Small);

    m_icon->setFixedHeight(iconSize);
    m_icon->setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));
    m_icon->setUseCursor(false);

    KIcon icon("konqueror");
    m_icon->setPixmap(icon.pixmap(iconSize, iconSize, (m_enabled ? QIcon::Normal : QIcon::Disabled)));

    m_icon->setToolTip(i18n("Konqueror Greasemonkey Userscripts Support"));
    m_statusBarEx->addStatusBarItem(m_icon, 0, true);

    connect(m_icon, SIGNAL(rightClickedUrl()), this, SLOT(slotIconMenu()));
    connect(m_icon, SIGNAL(middleClickedUrl()), this, SLOT(slotShowScriptManager()));
    connect(m_icon, SIGNAL(leftClickedUrl()), this, SLOT(slotToggleEnabled()));
}

/*
*
*/
void KHtmlUserscript::slotRemoveStatusbarIcon()
{
    if (m_icon != 0) {
        m_statusBarEx->removeStatusBarItem(m_icon);
    }
}

/*
*
*/
void KHtmlUserscript::slotIconMenu()
{
    if (!m_menu) {
        return;
    }

    m_menu->menu()->popup(QCursor::pos());
}

K_EXPORT_COMPONENT_FACTORY(khtml_userscript, KGenericFactory<KHtmlUserscript>("khtml_userscript"))

#include "khtml_userscript.moc"


